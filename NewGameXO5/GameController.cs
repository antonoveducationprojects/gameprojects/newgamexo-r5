﻿/*
 * Created by SharpDevelop.
 * User: guyver
 * Date: 27.11.2018
 * Time: 8:29
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace NewGameXO
{
	/// <summary>
	/// Description of ......
	/// </summary>
	/// 
	public enum GameState
	{
		inProgress,
		Winner,
		GameOver
	}
	public class GameController
	{
		int[,] GameMap= new int[3,3];
		public int currentStep {get;private set;}
		public GameState gameState {get;private set;}
		protected const int codeX=1,codeO=0,codeFree=-1;
		protected readonly int rectWidth;
		protected readonly int rectCenter;
//		protected readonly int lastRectCenter;
		public int WinnerCode {get; private set;}
		public Point[] Direction {get;private set;}
		protected Graphics gDev;
		protected GameFild gameFild;
		protected AbstractGameFigure[] figures=new AbstractGameFigure[2];
		
		
		public GameController(Graphics dev,int width)
		{
			gameState=GameState.inProgress;
			gDev=dev;
			currentStep=0;
			gameFild=new GameFild();
			figures[0]=new SimpleOFigure();
			figures[1]=new SimpleXFigure();
			ResetMap();
			Direction = new Point[2];
			rectWidth=width;
			rectCenter=width/2;
//			lastRectCenter=2*width+rectCenter;
		}
		
		protected void ResetMap()
		{
			for(int i=0;i<3;++i)
				for(int j=0;j<3;++j)
					GameMap[i,j]=codeFree;
		}
		
		private void SetDirection(int x1,int y1,int x2,int y2)
		{
			Direction[0].X=rectCenter+x1*rectWidth;
			Direction[0].Y=rectCenter+y1*rectWidth;
			Direction[1].X=rectCenter+x2*rectWidth;
			Direction[1].Y=rectCenter+y2*rectWidth;
		}
		
		public void DisplayMap(Graphics gDev)
		{
			int figureCode;
			gameFild.Display(gDev);
			for(int i=0;i<3;++i)
				for(int j=0;j<3;++j)
			    {
					figureCode=GameMap[i,j];				
					if(figureCode==codeFree)
						continue;
					figures[figureCode].Display(gDev,i*rectWidth,j*rectWidth);
				}
			if(gameState==GameState.Winner)
		    {
				var dir=Direction;
				Pen currentPen= new Pen(Color.Red,10);
				gDev.DrawLine(currentPen,dir[0],dir[1]);
			}
		}
		
		public void MakeJob(int x, int y)
		{
			if(gameState==GameState.GameOver)
				return;
			int i=x/rectWidth;
			int j=y/rectWidth;
			if(GameMap[i,j]!=codeFree)
				return;
			GameMap[i,j]=currentStep%2;
			currentStep++;
		}
		
		public bool ChangeStateToWinner(int j)
		{
			gameState=GameState.Winner;
			WinnerCode=j;
			return true;
		}
		
		public bool CheckWinner()
		{
			int[] checkRow,checkCol,checkDiag,checkDiag2;
			checkRow=new int[2];
			checkCol=new int[2];
			checkDiag=new int[2];
			checkDiag2=new int[2];
			for(int i=0;i<3;++i)
			{
				checkRow[0]=checkRow[1]=checkCol[0]=checkCol[1]=0;
				for(int j=0;j<3;++j)
				{
					if(-1!=GameMap[i,j])
						checkRow[GameMap[i,j]]++;
					if(-1!=GameMap[j,i])
						checkCol[GameMap[j,i]]++;
				}
				for(int j=0;j<2;++j)
				{
					if(3==checkRow[j])
					{
						SetDirection(i,0,i,2);
						return ChangeStateToWinner(j);
					}
					if(3==checkCol[j])
					{
						SetDirection(0,i,2,i);
						return ChangeStateToWinner(j);
					}
				}
				if(codeFree!=GameMap[i,i])
					checkDiag[GameMap[i,i]]++;
				if(codeFree!=GameMap[2-i,i])
					checkDiag2[GameMap[2-i,i]]++;
			}
			for(int j=0;j<2;++j)
			{
				if(3==checkDiag[j])
				{
					SetDirection(0,0,2,2);
					return ChangeStateToWinner(j);
				}
				if(3==checkDiag2[j])
				{
					SetDirection(2,0,0,2);
					return ChangeStateToWinner(j);
				}
			}
			if(9==currentStep)
			{
				gameState=GameState.GameOver;
				return true;
			}
			return false;		
		}
		public void ResetGame()
		{
			ResetMap();
			WinnerCode=-1;
			gameState=GameState.inProgress;
			currentStep=0;
		}
	}
}
